// Aggregation in MongoDB
// -- An aggregate is a cluster of things that hae come or have been brought together.

// Use Cases for Aggregations
// (1) You would like to know the total numer of fruits sold by a particular farm
// (2) Get the highest priced fruit sold per supplier
// (3) Get the average price of items sold by a Farm or Supplier


db.fruits.insertMany([
    {
        name: "Apple",
        color: "Red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines", "US"]
    },
    {
        name: "Banana",
        color: "Yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines", "Ecuador"]
    },
    {
        name: "Kiwi",
        color: "Green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },
    {
        name: "Mango",
        color: "Yellow",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines", "India"]
    }
]);



// Using the aggregate method
/* Syntax:
		db.collection.aggregate([]);
*/



/* -"$match" method -- associated with aggregate. Will hold the criteria/condition for it to go to the next aggregation process
Syntax:
		{ $match: { field: value } }

	-"$group" method -- will look for properties and will group them based on condition given
Syntax:
		{ $group: {_id: "$value", fieldResult: "valueResult"} }
*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } }}  //groups supplier id with added total array -- it computes the total sum of stocks
]);



// Field Projection with Aggregation

//	"$project"
/*	Syntax:
		{ $project: { field: 1 or 0 } } //set or unset ONLY the ID
*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } }},
	{ $project : { _id: 0 } }
]);



// Sorting aggregated results

//	"$sort"
/*	
	Syntax:
		{ $sort : { field: 1 or 0 } } 1 for proper order, -1 for reversed
*/

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } }},
	{ $sort : { total: -1 } }
]);



// Aggregating results based on array fields

//	"$unwind" -- Deconstructs an array field from the input documents to output a document for each element
/*
	Syntax:
		{ $unwind : field }
*/

db.fruits.aggregate([
	{ $unwind : "$origin" }
]);


// Dispalys fruit documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
	{ $unwind : "$origin" },
	{ $group : { _id : "$origin", kinds: { $sum: 1 } } }
]);